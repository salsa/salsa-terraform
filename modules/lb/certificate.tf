resource "tls_private_key" "example" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "example" {
  key_algorithm   = tls_private_key.example.algorithm
  private_key_pem = tls_private_key.example.private_key_pem

  validity_period_hours = 730 * 24

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]

  dns_names = [
    "${var.domain}.example",
  ]

  subject {
    common_name  = var.domain
    organization = "ACME Inc"
  }
}

resource "random_pet" "certificate" {
  keepers = {
    certificate = tls_self_signed_cert.example.cert_pem
  }
}

resource "google_compute_ssl_certificate" "example" {
  name = "salsa-example-${random_pet.certificate.id}"

  private_key = tls_private_key.example.private_key_pem
  certificate = tls_self_signed_cert.example.cert_pem

  lifecycle {
    create_before_destroy = true
  }
}
