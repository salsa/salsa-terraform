resource "google_compute_instance_group" "vm" {
  name = "salsa-${var.env}-monitor"
  zone = var.zone

  instances = [
    google_compute_instance.vm.self_link,
  ]

  named_port {
    name = "http"
    port = "8080"
  }
}
