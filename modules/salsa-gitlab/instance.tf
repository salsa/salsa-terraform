resource "google_compute_instance" "gitlab" {
  name         = "salsa-${var.env}"
  machine_type = var.instance_type
  zone         = var.zone

  allow_stopping_for_update = true

  tags = [
    "http-server",
    "https-server",
  ]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      type  = "pd-ssd"
      size  = 30
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}
