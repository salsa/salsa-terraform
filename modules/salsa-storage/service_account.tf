resource "google_service_account" "gitlab" {
  account_id = "gitlab-storage"
  project    = var.project
}
