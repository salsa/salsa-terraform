module "storage" {
  source = "../../modules/salsa-storage"

  env = "prod"

  project = "debian-salsa-prod"

  location      = "us"
  storage_class = "MULTI_REGIONAL"

  origin          = "https://salsa.debian.org"
  origin_registry = "https://registry.salsa.debian.org"
}
