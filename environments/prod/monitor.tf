module "monitor" {
  source = "../../modules/salsa-monitor"

  env = "prod"

  project = "debian-salsa-prod"
  zone    = "us-west1-c"

  disk_size     = 50
  instance_type = "e2-small"
}
